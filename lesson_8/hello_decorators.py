#coding: utf-8

from datetime import datetime

def decor(func):
    def newFunc():
        print "decor Func: " , datetime.now(), func
        func()
    return newFunc

@decor
def func():
    print "oppa pa"

@decor
def func2():
    print "func2"

func()
func2()

def smart_rstrip(str):
    if len(str) > 0 and str[-1] == '\n':
        if len(str) > 1 and str[-2] == '\r':
            return str[:-2]
        else:
            return str[:-1]
    else:
        return str

def filer(func):
    def do_file_open(filename):
        try:
            f = open(filename, 'r')
            func()
        except IOError:
            print "can not open file %s" % filename
        return do_file_open


#=================Ок с этим разобрались
@filer
def find_django(filename):
    fname = filename.split('.')
    new_f = open(fname[0] + "_template." + fname[1], 'w')
    other_f = open(fname[0] + "_1.6." + fname[1], 'w')
    for line in f:
        counter += line.count("Django")
        if smart_rstrip(line).endswith(':'):
            counter_doubledot += 1
        new_line = line.replace("Django", "{0}")
        new_format_line = new_line.format("Django 1.6")
        new_f.write(new_line)
        other_f.write(new_format_line)

    print "\"Django\" word is totally found %i times, \":\" is found %i" % (counter, counter_doubledot)

find_django("1.txt")
