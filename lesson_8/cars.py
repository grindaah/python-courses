#coding: utf-8

# Применим декоратор к кдассу
# Если у класса нет атрибута model, мы инициализируем этот атрибут с None
def dekor(cls):
    if hasattr(cls, 'model'):
        return cls
    class dek_class(cls):
        model = "NoNAME"

    return dek_class

def dekor_with_param(cls, isDay):
    if isDay:
        if hasattr(cls, 'model'):
            return cls
        class dek_class(cls):
            model = "NoNAME"
        return dek_class
    else:
        return cls


@dekor_with_param(True,)
class Car(object):
    max_speed = 130
#    model = "Lada"
    speed = 90
    def __str__(self):
        return "Car"

    def drive(self, t):
       L = self.speed * (t / 3600.0)
       print "Проехали: %s км за %s сек" % (L, t)

car = Car()
print car.model
