#coding: utf-8

import socket
from random import randint

def gen_garbage():
    result = "".join([chr(randint(31,99)) for i in range(randint(1,10))])
    print result
    return result

def gen_pythons():
    f = randint(1,10)
    print f
    result = ""
    for i in range(f):
        result += "python"
        result += gen_garbage()
    return result

HOST = "localhost"
PORT = 8000

#AF_INET - тип связи
#SOCK_STREAM - тип обмена (TCP)
#SOCK_DGRAM  - тип обмена (датаграммами) (UDP)

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((HOST, PORT))

send_str = gen_pythons()
sock.send(send_str.encode("utf-8"))
result = sock.recv(1024).decode("utf-8")

sock.close()

print u" Получено:", result
