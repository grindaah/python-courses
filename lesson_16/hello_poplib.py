#coding: utf-8

import argparse
import poplib
import imaplib, email

password = "123"

'''    parser = OptionParser()
    parser.add_option("-h", "--host", dest="host", default="pop.yandex.ru"
            help="host address of your e-mail box")
    parser.add_option("-p", "--port", dest="port", default="25",
            help="port of your mail service")
    parser.add_option("-u", "--user", dest=username. default="testpydev@yandex.ru"
            help="username for your mail account")'''

host="pop.yandex.ru"
port=110
login="testpydev@yandex.ru"
password='testpassword123'


def main():
    box = poplib.POP3(host, port) # в принципе, если порт 110, то его можно и не указывать
    # если вам нужно подключаться с использованием SSL, то потребуется питон не ниже 2.6 и выглядеть это будет так:
    # box = poplib.POP3_SSL(server, port) # в принципе, если порт 995, то его можно и не указывать
    box.user(login)
    box.pass_(password)

    response, lst, octets = box.list()
    print "DEBUG: Total %s messages: %s" % (login, len(lst))

    for msgnum, msgsize in [i.split() for i in lst]:
        (resp, lines, octets) = box.retr(msgnum)
        msgtext = "n".join(lines) + "nn"
        message = email.message_from_string(msgtext) # если надо, парсим письмо и получаем объект со всеми данными
        print message["to"]
        print message["subject"]
        print(msgtext)
        # box.dele(msgnum) # если надо - удаляем с сервера письмо
        # реально оно будет удалено только после закрытия ящика,
        # а эта функция производит пометку письма как удалённого
        # и пока ящик не закрыт, письмо можно восстановить

def main2():
    box = imaplib.IMAP4(host, port)
    box.login(login, password)
    box.select() # выбираем папку, по умолчанию - INBOX
    typ, data = M.search(None, 'ALL') # ищем письма
    # None здесь говорит о том, что нам всё равно, в какой кодировке искать письма
    # ALL - искать все письма
    # search(None, 'FROM', '"LDJ"') или search(None, '(FROM "LDJ")') - искать письма со строкой LDJ в поле From
    for num in data[0].split() :
        typ, data = M.fetch(num, '(RFC822)')
        print 'Message %sn%sn' % (num, data[0][1])
    
    box.close()
    box.logout()


if __name__ == "__main__":
    main2()
