#coding: utf-8

# CSV = comma separate values
# Dialekt?

import csv

for row in csv.reader(open("sample.csv")):
    row.sort()
    for el in row:
        print el


# DictReader можно передать fieldnames - это будут имена колонок
# 
for row in csv.DictReader(open("sample.csv")):
    for el in row:
        print el


# Еще есть writer
