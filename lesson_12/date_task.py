#coding: utf-8

import re

t6 = r'[0-3]+[0-9]+\.[0-1]+[0-2]+\.[0-9]{4}'
r6 = re.compile(t6)
date1 = "123891239123"
date2 = "17302013"
date3 = "17.02.2914"
date4 = "94.12.2222"
date5 = "01.01.2014"
date6 = "01.1.2014"
date7 = "00.00.0000"

t_sed = r'([0-3]+[0-9]+)\.([0-1]+[0-2]+)\.([0-9]{4})'
sed = re.compile(t_sed)
mo = sed.search(date5)

print mo.groups()
print mo.expand(r'\2-\1-\3')

