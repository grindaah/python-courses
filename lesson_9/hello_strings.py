#coding: utf-8
import string

# методы строки
str = "adasdf   "

print str.upper()
print str.rstrip()
print str.lstrip()
print str.strip()

#  шаблоны
#    "     " % .....
#    str.format()

a = 3812.1212
b = 12.12
print "%3.2f,%12.2f" % (a,b)

name = "Alex"
names = [name, "" , "aaaa", "adsasdadas"]
#print " %(name)s     " % -тут  dict

#d, i - целое число
#u - беззнаковое цкелое
#f - <->m.ddddddd - пл.точка
#E - <->m.ddddddd - научный вид
#g,G - если E < 4 - то обычный, иначе - научный
#s - str, строка
#r - repr( может быть raw?)
#c - char

#%(имя ключа)s
#%(-символ выравнивания по левому краю)s

print "%-32d" % a
print "%32d" % a
print "%+d" % a
print "%000000d" % a

# .X - либо количество цифр после запятой, либо макс. кол-во символов
# X.__  - общее количество символов под вывод
print "%5.2s" % str

# Задание: вывод таблицы

dicts = [ {"name": "Teddybear", "weight": 24, "fussy" : 80.8}
        ,{"name": "Teddy", "weight": 24, "fussy" : 80.8}
        ,{"name": "Teddybear", "weight": 20004, "fussy" : 100.0}
        ,{"name": "Teddybeaqqqr", "weight": 24, "fussy" : 5.8}
        ,{"name": "Teddybear", "weight": 24, "fussy" : 80.8}
        ]

isalpha = [True, True, True]
isdigit = [True, True, True]
istitle = [True, True, True]

for dict in dicts:
    print "%(name)10.10s | %(weight)10.10s | %(fussy)6.2f %%" % dict
    isalpha[0] = isalpha[0] and dict["name"].isalpha()
    isdigit[0] = isdigit[0] and dict["name"].isdigit()
    istitle[0] = istitle[0] and dict["name"].istitle()

    #wgt = str(dict["weight"])
    #isalpha[1] = isalpha[1] and wgt.isalpha()
    #isdigit[1] = isdigit[1] and str(dict["weight"]).isdigit()
    #istitle[1] = istitle[1] and str(dict["weight"]).istitle()

    #isalpha[2] = isalpha[2] and str(dict["fussy"]).isalpha()
    #isdigit[2] = isdigit[2] and str(dict["fussy"]).isdigit()
    #istitle[2] = istitle[2] and str(dict["fussy"]).istitle()

#print isalpha

print "="* 80
print "alpha: %6s %10s %10s" % (isalpha[0], isalpha[1], isalpha[2])
print "digit: %6s %10s %10s" % (isdigit[0], isdigit[1], isdigit[2])
print "title: %6s %10s %10s" % (istitle[0], istitle[1], istitle[2])

print istitle
print len(istitle)
print "title: %6s %10s %10s" % tuple(istitle)
print str.center(12) # параметр - кол-во символов, в которых центрировать
#.expandtabs() - меняет \t на пробелы

#.find() - если не находит возв -1
#.index9) - Если не находит raise ValueError
#.isalnum() - проверяет, состоит ли строка только из букв и цифр
strdd = "2892.1"
print str.isalnum()
print str.isalpha()
print str.isdigit()

print strdd.isdigit()
#print strdd.isdecimal()   - в третьем питоне
print strdd.islower()
#print strdd.isnumeric()   - в третьем питоне
print str.istitle() # - начинается ли с заглавной
