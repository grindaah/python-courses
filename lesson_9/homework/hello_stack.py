#coding: utf-8

class Stack(object):
    def __init__(self, size):
        '''top, size и сам массив s скрыты'''
        self.__top = 0
        self.__size = size
        self.__s = [0 for i in range(size)]

    def pop(self):
        '''сам элемент удален НЕ будет, просто перемещаем указатель'''
        if not self.check_empty():
            self.__top -= 1
            return self.__s[self.__top]
        else:
            print "Warning: stack empty"
            return False

    def push(self, obj):
        '''Можно было бы использовать insert в реализациях, где размер
        list можно ограничить'''
        if not self.check_full():
            self.__s[self.__top] = obj
            self.__top += 1
        else:
            print "Stack overflow :P"

    def check_full(self):
        return self.__top == self.__size

    def check_empty(self):
        return self.__top == 0

    def __size__(self):
        return self.top

    def __str__(self):
        return str(self.__s)



stack1 = Stack(11)
stack2 = Stack(12)
stack3 = Stack(13)
stack4 = Stack(14)

lst = range(15)

for item in lst:
    stack1.push(item)
    stack2.push(item)
    stack3.push(item)
    stack4.push(item)

print "%s \n" % stack1
print "%s \n" % stack3

while stack3.pop():
    print "pop1 %s" % stack1.pop()
    print "pop2 %s" % stack2.pop()
    print "pop4 %s" % stack4.pop()

stack4.push(0)
stack4.push(0)

print stack4.pop(), stack4.pop(), stack4.pop()
print stack3.pop()
