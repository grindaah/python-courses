#coding: utf-8

class Queue:
    def __init__(self, size):
        self.__head = 0
        self.__tail = 0
        self.__size = size + 1
        self._s = [0 for i in range(size+1)]

    def _check_full(self):
        return self.__tail - self.__head == self.__size-1 or self.__head - self.__tail == 1

    def _check_empty(self):
        return self.__tail == self.__head or self.__head - self.__tail == self.__size - 1

    def _inc(self, x):
        if x == self.__size-1:
            x = 0
        else:
            x += 1
        return x


    def enqueue(self, x):
        if not self._check_full():
            self.__tail = self._inc(self.__tail)
            self._s[self.__tail] = x
            print self._s
        else:
            print "queue full"

    def dequeue(self):
        if not self._check_empty():
            self.__head = self._inc(self.__head)
            return self._s[self.__head]
        else:
            print "queue empty"
            print self.__head
            print self.__tail

q = Queue(3)
q.enqueue(1)
q.enqueue(2)
q.enqueue(3)
q.enqueue(4)

print q.dequeue()
print q.dequeue()
print q.dequeue()
print q.dequeue()

q.enqueue(1)
q.enqueue(2)
print q.dequeue()
q.enqueue(3)
q.enqueue(4)
print q.dequeue()
print q.dequeue()
