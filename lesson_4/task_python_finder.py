#coding: utf-8
import os

def print_all_files():
# we can use "walk" for example
#    for d1,d2,filenames in os.walk('.'):
#        for filename in filenames:
#            print filename
    for filename in os.listdir('.'):
        print filename


def check_txt_file(filename):
    if filename[-4:] == ".txt":
        return True
    else:
        return False

def find_python(str):
    counter = 0
    i = 1
    while i != 0:
        try:
            i = str.lower().index("python")
        except ValueError: 
            break
        counter += 1
        str = str[i+6:]

    return counter

print check_txt_file('blabla.txt')
print check_txt_file('adadtxt')

print "OK, checked it's working, going on...."
print find_python("afdpythonjklasdflkaj pythonadsflkkjfljakfsljksaf python")
print find_python("python")

counter = 0
filescounter = 0
txtfilescounter = 0

print_all_files()

def find_python_gen():
    #chk_txt_line = lambda str: True if str.endswith('.txt') else False
    # Или:
    python_lines_count = [line.count("python") for f in os.listdir('.')
        for line in open(f)
        if f.endswith('.txt')
        if line.lower().count("python")]

    print python_lines_count
    # ========================== Конец изврата ======================
    all_files_in_dir = os.listdir('.')
    print all_files_in_dir
    txtfiles = [txtfile for txtfile in os.listdir('.') if txtfile.endswith('.txt')]
    print txtfiles
    for txt in txtfiles:
        python_lines = [line for line in open(txt) if line.lower().count("python")]
    for l in python_lines:
        yield str(l.lower().count("python")) + l

    # Первый рабочий вариант
    #for txt in os.listdir('.'):
    #    if txt.endswith('.txt'):
    #        try:
    #            f = open(txt, 'r')
    #            for line in f:
    #                if line.lower().count("python") > 0:
    #                    yield line + " : " + str(line.lower().count("python")) + " in file: " + txt
    #        except IOError:
    #            print "IOError"

for l in find_python_gen():
    print l


