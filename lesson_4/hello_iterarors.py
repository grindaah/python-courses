#coding: utf-8
#чтобы объект стал итератором, достаточно метода next()

#Для того. чтобы пользоваться "in" необходимо добавить метод __iter__()

mo = {'name': "Alexandra", "age": 41, "child": None}
child = {"name": "Alex", "age": 8, "parent": mo, "child": None}
mo["child"] = child

for a in mo:
    print "key:", a, "value:", mo[a]
