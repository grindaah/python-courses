#coding: utf-8

#Сопрограммы 
# также содержат def и (yield) но еще и бесконечный цикл

def sopr():
    while True:
        gamma = (yield)
        print gamma

so = sopr()

#Attention: нельзя обращаться к сопрограмме, до next()
# То, что в send запишется в (yield)
so.next()
so.send("7")
#....
so.send("2")

