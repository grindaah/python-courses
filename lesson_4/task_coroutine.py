#coding: utf-8
import os

#coroutine
# Поиск слова "python"
def find_python():
    while True:
        line = (yield)
        count_python = line.lower().count("python")
        if count_python:
            print str(count_python) + " :" + line

# Поиск текстового файла
def find_txt():
    while True:
        flname = (yield)
        if flname.endswith(".txt"):
            fp = find_python()
            fp.next()
            for line in open(flname):
                fp.send(line)

#Второй вариант
def find_file(target):
    while True:
        flname = (yield)
        if flname.endswith(".txt"):
            for line in open(flname):
                target.send(line)

def fib():
    fib_sum = 0
    while True:
        new_member = (yield)
        fib_sum += new_member
        print fib_sum
          
ft = find_txt()
ft.next()

fp = find_python()
fp.next()

ff = find_file(fp)
ff.next()
for filename in os.listdir('.'):
    ft.send(filename)
print "="*80
for filename in os.listdir('.'):
    ff.send(filename)

print "="*80
print "Fibonacci sequence:"
fib_fun = fib()
fib_fun.next()

f = 1
result = 1
#while result < 1000:
#    result = fib_fun.send(result)
for f in fib_fun.send(f):
    pass

# x = (yield y) - тоже легитимная запись
# y - будет генератором
