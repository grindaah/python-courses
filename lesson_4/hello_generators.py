#coding: utf-8
lst = [5,6,7,7,8]
print "Список1:", lst
#Attention: list comprehension работает быстрее чем
lst2 = [2*a for a in lst if a > 6]
print "Список2:", lst2

#То же самое, но в круглых скобках, это вовсе не кортеж, а генератор
#а что такое генератор... см. ниже
lst3 = (2*a for a in lst if a > 6)

#Генератор - функция, создающая объект перечисления

def gen(objects):
    for obj in objects:
        yield obj

for g in gen(lst2):
    print g

def gen_file():
    for line in open("1.txt"):
        if "python" in line:
            yield line


for l in gen_file():
    print l
