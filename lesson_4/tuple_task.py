#coding: utf-8

A_obj = ("A", 1, 17, -9)
B_obj = ("B", 7, 38, 19)
C_obj = ("C", 12, 7, 8)

obj_list = [A_obj, B_obj, C_obj]
y_coords = []

# We can crete list of strings here and then join UPPER for proper index
def variant1():
    for obj in obj_list:
        y_coords.append(obj[2])

    upper_index = y_coords.index(min(y_coords))

    for obj in obj_list:
        if obj_list.index(obj) == upper_index:
            print "<%s>: %s, %s, %s : UPPER" % obj
        else:
            print "<%s>: %s, %s, %s " % obj

string_list = []
def variant2():
    for obj in obj_list:
        y_coords.append(obj[2])
        string_list.append("<%s>: %s, %s, %s " % obj)

    upper_index = y_coords.index(min(y_coords))
    string_list[upper_index] = string_list[upper_index] + ": UPPER"

    print "\n".join(string_list)

def variant3():
    "Список из тел, которые выше 20"
    upper_objects = [a for a in obj_list if a[2] > 20]

    print "Список из тел, которые выше 20"
    print upper_objects

variant1()
variant2()
variant3()
