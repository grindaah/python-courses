#coding: utf-8

from random import randint
from random import choice
import string

class Vertex(object):
    def __init__(self,val):
        self.key = val

        self.name = string.ascii_lowercase[choice(range(26))] + str(randint(0,99))
        self.neighbors = set([])

    def __lt__(self, value):
        return self.key < value.key

    def __str__(self):
        return "%s: %s" % (self.name, self.neighbors)

    def add(self, vertex):
        self.neighbors.append(vertex)
        vertex.connect(self)

    def connect(self, vertex):
        self.neighbors.append(vertex)



def gen_rand_graph(num):
    for i in range(num):
        V = Vertex(i)
        print V

gen_rand_graph(10)
