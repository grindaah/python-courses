#coding: utf-8
import os

def check_txt_file(filename):
    if filename[-4:] == ".txt":
        return True
    else:
        return False

# Отрезаем __только__ символы перевода строки и __только__ по одному,
# сохраняя структуру документа и не отрезая пробелы
def smart_rstrip(str):
    if len(str) > 0 and str[-1] == '\n':
        if len(str) > 1 and str[-2] == '\r':
            return str[:-2]
        else:
            return str[:-1]
    else:
        return str

counter = 0
counter_doubledot = 0

filename = "1.txt"

try:
    f = open(filename, 'r')
    fname = filename.split('.')
    new_f = open(fname[0] + "_template." + fname[1], 'w')
    other_f = open(fname[0] + "_1.6." + fname[1], 'w')
    for line in f:
        counter += line.count("Django")
        if smart_rstrip(line).endswith(':'):
            counter_doubledot += 1
        new_line = line.replace("Django", "{0}")
        new_format_line = new_line.format("Django 1.6")
        new_f.write(new_line)
        other_f.write(new_format_line)
except IOError:
    print "error reading file %s" % filename

print "\"Django\" word is totally found %i times, \":\" is found %i" % (counter, counter_doubledot)
