#coding: utf-8

from random import randint

def compr(elem, lst):
    i = 0
    result = []
    while elem > lst[i]:
        result.append(lst[i])
        i += 1
    return i, result

def merge(lst1, lst2):
    result = []
    i,j = 0, 0

    #if lst1[len(lst1)-1] < lst2[0]:
    #    result = lst1 + lst2
    #    return result

    while len(result) < len(lst1) + len(lst2):
        if i == len(lst1)-1 and lst1[i] < lst2[j]:
            result.append(lst1[i])
            result += lst2[j:]
            return result
        if j == len(lst2)-1 and lst2[j] < lst1[i]:
            result.append(lst2[j])
            result += lst1[i:]
            return result

        if lst1[i] < lst2[j]:
            result.append(lst1[i])
            i+=1
        else:
            result.append(lst2[j])
            j+=1
    return result

def dividelist(lst):
    i = len(lst) / 2
    return lst[:i], lst[i:]

def merge_sort(lst):
    if len(lst) > 1:
        return merge(merge_sort(lst[:len(lst)/2]), merge_sort(lst[len(lst)/2:]))
    else:
        return lst

lst1 = [40,5,4,7]
lst2 = [240,15,24,37,8,72,820,312,123,166]

#print merge(lst1, lst2)

lst4, lst5 = [1,5], [4,8]

print merge(lst4,lst5)

print merge_sort(lst1)
print merge_sort(lst2)

print "Random list: "
rnd_lst = [randint(-400,20000) for i in xrange(20)]
print rnd_lst
print "Merged:"
sorted_lst = merge_sort(rnd_lst)
print sorted_lst
rnd_lst.sort()
if rnd_lst == sorted_lst:
    print "Test passed"
