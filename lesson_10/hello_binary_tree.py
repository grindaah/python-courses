#coding: utf-8

class Vertex(object):
    def __init__(self, parent, val):
        self.key = val
        self.parent = parent
        self.lChild = None
        self.rChild = None

#class Edge(object):
#    value #(вес) - пока не надо
#    startVertex
#    endVertex

class BinaryTree:
    def __init__(self, depth):
        self.root = None
        self.level = 1

    def search(self, current, next, key):
        if next != None:
            self.level += 1
            if next.key > key:
                self.search(next, next.lChild, key)
                return
            elif next.key < key:
                self.search(next, next.rChild, key)
                return

        if current.key > key:
            current.lChild = Vertex(current, key)
            print "Found node: level %d - left, val: %d" % (self.level + 1, key)
            return
        else:
            current.rChild = Vertex(current, key)
            print "Found node: level %d - right, val: %d" % (self.level + 1, key)
            return

    def insert(self, x):
        next = self.root

        if self.root == None:
            self.root = Vertex(None, x)
            print "root inserted %d" % x
            return

        self.level = 1

        if x < next.key:
            self.search(next, next.lChild, x)
        else:
            self.search(next, next.rChild, x)

    def find(self, x):
        if self.root.key == x:
            return self.root
        nd = self.root
        return self.find_from_node(nd, x)

    def find_from_node(self, nd, x):
        if nd == None:
            return nd
        if x > nd.key:
            nd = nd.rChild
            self.find_from_node(nd, x)
            return
        elif x < nd.key:
            nd = nd.lChild
            self.find_from_node(nd,x)
            return
        else:
            return nd

btree = BinaryTree(5)

btree.insert(5)
btree.insert(10)
btree.insert(20)
btree.insert(30)
btree.insert(11)
btree.insert(6)
btree.insert(3)

if btree.find(4) == None:
    print "4 not found"
else:
    print "4 found"
if btree.find(20) == None:
    print "20 found"


