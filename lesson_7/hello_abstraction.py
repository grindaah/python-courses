#coding: utf-8

from abc import ABCMeta, abstractmethod, abstractproperty

class Foo(object):
    __metaclass__ = ABCMeta
    @abstractmethod
    def method(self):
        pass
    @abstractproperty
    def name(self):
        pass

class Bar(Foo):
    def method(self):
        pass
    def name(self):
        pass

b = Bar()

# Метаклассы:
__metaclass__ = type

#class Foo(Metod):
#    pass

# структура НОВОГО класса

class_name = 'Foo1'
class_body = "pass"
class_dict = {}
class_parents = (object,)
exec(class_body, globals(), class_dict)
Foo1 = type(class_name, class_parents, class_dict,)

ff = Foo1()
print type(Foo1)


class DocMeta(type):
    def __init__(self, name , bases, dict):
        for key, value in dict.items():
            if key.startswith('    '):
                continue
            if not hasattr(value, '__call__'):
                continue
            if not getattr(value, '__doc__'):
                raise TypeError("%s must have docstring" % key)
        type.__init__(self, name, bases, dict)

class Documeneted:
    __metaclass__ = DocMeta

class FooBar(Documented):
    def spam(self):
        "wtf??!?!"
        pass


