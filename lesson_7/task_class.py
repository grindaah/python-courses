#coding: utf-8
from math import trunc

class My_time:
    time_types = ("hours", "minutes", "seconds")
    time_converters = (60.0, 60.0, 60.0)
    def __init__(self, value = 0.0, timetype = time_types[0]):
        if timetype not in self.time_types:
            self.timetype = self.time_types[0]
        else:
            self.timetype = timetype

        if (self.timetype == "hours"):
            self.hours = int(value)
            if value > 1:
                fminutes = (value % trunc(value)) * 60.0
            else:
                fminutes = value * 60.0
        elif (self.timetype == "minutes"):
            if value >= 60:
                self.hours = value / 60
                fminutes = value % 60.0
        elif (self.timetype == "seconds"):
            if value >= 3600:
                self.hours = value / 3600
                fminutes = value / 60.0
        self.minutes = int(fminutes)
        if fminutes > 1:
            self.seconds = (fminutes % trunc(fminutes)) * 60.0
        else:
            self.seconds = fminutes * 60.0
    def __str__(self):
        return "%s hrs %s min %.3f seconds" % (self.hours, self.minutes, self.seconds)

class Car(object):
    max_speed = 130
    model = "Lada"
    speed = 90
    def __str__(self):
        return "Car"

    def drive(self, t):
        L = self.speed * (t / 3600.0)
        print "Проехали: %s км за %s сек" % (L, t)

class complexCar(Car):
# --- Пример из седьмого урока с property
    @property
    def optnal_eqpt(self):
        return self.__optnal_eqpt

    @optnal_eqpt.setter
    def optnal_eqpt(self, eqpt):
        print "setting optional equipment: %s" % (eqpt)
        self.__optnal_eqpt = eqpt

    @optnal_eqpt.deleter
    def optnal_eqpt(self):
        del (self.__optnal_eqpt)

    capacity = 42
    consume = 10

    def __init__(self, Fuel):
        self.fuel = Fuel
        self.speed = self.speed * self.fuel.kpd

    def check_enough(self):
        return (self.capacity - self.consume) > 0

    def TestingRun(self):
        print  "==============Забег..."
        if not self.check_enough():
            print "Не хватило топлива"
            L = self.capacity * 100.0 / float(self.consume)
            print "Проехали: %.2f км" % L
            self.capacity = 0
            tm = My_time(L / self.speed)
        else:
            self.capacity = self.capacity - self.consume
            tm = My_time(100.0 / self.speed)
        print "speed = %s" % self.speed
        print "Fuel type: \t%s" % self.fuel.fuelType
        print "capacity: \t%s" % self.capacity
        print "100 km run: \t%s" % tm



#Пример ассоциации:
class GazStation:
    fuels = {}
    def __init__(self, name = "Alex 0_x?", *params):
        self.name = name
        if len(params) > 0:
            self.fuels[params]


c = Car()
c.drive(360)
c.speed = 200
c.drive(3600)

class Fuel:
    kpd = 0.95
    def __init__(self, kpd = 0.95, fuelType = "gazoline"):
        self.kpd = kpd
        self.fuelType = fuelType


gazoline = Fuel()
diesel = Fuel(1.00, "dizzel")
gaz92 = Fuel(0.92)

car1 = complexCar(gazoline)
car2 = complexCar(diesel)
car3 = complexCar(gaz92)

car1.TestingRun()
car2.TestingRun()
car3.TestingRun()

car3.consume = 100
car3.TestingRun()
car3.check_enough()

car3.optnal_eqpt = ["Luggage", "Nitro", "Driver"]
