#coding: utf-8

# Агрегация и ассоциация:
# Агрегация: has A
# Ассоциация: use A

#static methods:

@staticmethod
def doGlobally():
    pass

# no need to construct class
#SomeClass.doGlobally

# атрибуты класса, объявленные в теле классе, являются статическими и 
# мы имеем к ним доступ

# Метод класса
@classmethod
class SomeClass:
    def now(cls):
        pass
# можно использовать:
#SomeClass.now()
#или
#s_c = SomeClass()
#s_c.now()

#жызненный пример
class A:
    @classmethod
    def clone(cls):
        return cls

class B (A):
    pass

a = A()
b = a.clone()
# теперь в b лежит содержимое а, но типизируется как b

# пример с датой
import time
class Date(object):
    def __init__(self, val):
        self.value = val
    @classmethod
    def now(cls):
        t = time.localtime()
        return cls(t)

class EuroDate(Date):
    def __str__(self):
        return "%s %s %s" % (self.value.tm_year, self.value.tm_mon, self.value.tm_mday)


val = time.localtime()
print Date(val).now()
print EuroDate(val)
print EuroDate(val).now()

#Свойства в их новом синтаксисе
class C(object):
    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        print "Setting X:%s" % x
        self.__x = x

    @x.deleter
    def x(self):
        del (self.__x)

c = C()

c.x = 10


# Создание класса
#def __new__(cls, * args, **kwargs):
#    pass
# вызывается при создании

#!!! Слабые ссылки
import weakref
a = weakref.ref(c)
del c

print a->x

# ограничения:
# в новом классе будут ограничены имена атрибутов класса
__slots__ = ('name', 'age',)
#тогда
SomeObj.name = "blabal" #можно
SomeObj.adfafd = 21212121 # нельзя
# __slots__ заполняет __dict__ объекта
