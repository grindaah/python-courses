#coding: utf-8
line = "ASCII"

def func(line):
    return line.lower()

# При вызове - новый объект-ссылка на iine
func(line)

def func2(a,b = 7):
    return a + b

func2(5)

#Attention: обязательные параметры должны быть расположены слева
#Так нельзя: 
#def func4(x,*args, y):
#    pass
def func4(x, y = 7, *args):
    print x, y, args

func4(10,20)

def func3(*args):
    print "We have multiple aguments:"
    for ln in args:
        print ln

func3(5,6,7,5,4,3,2,1)

#Attention! print по-разному работает в 2 и третьей версиях
#мы можем сделать from __future__ import print_functionality
print("A", 3, 4)

#Именованные аргументы
def draw(x,y=6, length = 20):
    pass

draw(1, length = 11)


#можно задавать значения по умолчанию между ** ???
def func_with_dict(x=0,y=0, **args):
    print args["Key"]

#Он даже понимает, что надо значение Key передать в словарь!
func_with_dict(4,Key="secret_key")


def mt(** params):
    fgcolor = params.pop('fgcolor', "black")
    print params
    if params:
        raise TypeError("Нет цвета %s" % params)

colors = {'fgcolor' : "red"}
print type(colors)
#Так нельзя: (мы не можем передавать заранее сформированный словарь в качестве params
#Почему? не спрашивай, просто делай ;)
#mt(colors)
#Так можно
mt(fgcolor='red')

a = 12

#Attention: нельзя использовать global для совпадающих локальных имен
def f(b):
    global a
    a = 13
    print "a = %i" % a
f(a)
print "a = %i" % a

# Функция в функции или non local...
# Пример замыкания в python
def f():
    a = 7
    def f1():
        #Третий питон может использовать ключевое слово nonlocal для изменения a
        print a
    return f1

x = f()
print x
x()

# Если фунеция ничего не возвращает, по умолчанию она возвращает None

# Пример замыкания
def countdown(start):
    n = start
    def display():
        print "Осталось %i" % n
    while n > 0:
        display()
        n-=1
    return display

sch = countdown(14)
sch()

