#coding: utf-8

#Лямбда - простое выражение без создания промежуточных данных
#!Attention: plus - это функция
plus = lambda x,y: x + y

print plus(1,2)

names = ["alex", "Boris", "Sergey", "kate", "nadja", "Tanja"]

print names.sort()

print names.sort(key = lambda n: n.lower())

#Можно использовать синтаксис г
sqr_list = lambda lst: [n*n for n in lst]

digit_list = [1,2,3,4,5,6]
print sqr_list(digit_list)

#Адская конструкция, вряд ли стоит использовать более одного if-else в лямбдах
only_sum = lambda lst: lst[1] + lst[2] if lst[0] == "plus" else lst[1] - lst[2] if lst[0] == "minus" else 0
sample_lst = ["plus", 1, 2]
sample_lst2 = ["minus", 4, 5]

#calcer task (callback - function)
def plus(a,b,calcer=None):
    if calcer:
        return calcer(["plus",a,b])
    return 0

print "calcer result: ", plus(1,2,only_sum)

print only_sum(sample_lst)
print only_sum(sample_lst2)


def calcer_fun(lst):
    if lst[0] == "plus":
        return lst[1] + lst[2]
    elif lst[0] == "minus":
        return lst[1] - lst[2]
    elif lst[0] == "multiply":
        return lst[1] * lst[2]
    elif lst[0] == "divide":
        return lst[1] * lst[2]
    return 0

sample_lst2[2] = 11
print calcer_fun(sample_lst2)

sample_lst[0] = "multiply"
print calcer_fun(sample_lst2)
