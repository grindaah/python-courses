#coding: utf-8

#Словарик

from sys import argv

dict_file = {"sun": {"noun": "Солнце", "verb": "", "adj": "", "transcription": "Сань"},
        "like": {"noun": "", "verb": "нравиться", "adj": "подобный чему-то", "transcription": "лайк"},
        "warm": {"noun": "", "verb": "греть", "adj": "теплый", "transcription": "ворм"},
        "rest": {"noun": "отдых", "verb": "отдыхать", "adj": "", "transcription": "рест"},
}

requests = ("noun", "verb", "adj", "transcription")

def is_first(iterator, container):
    return container.index(iterator) == 0

# TODO
#    Еще способ создавать лист запросов разом
#    rq_list = [key for key in other_keys if key in ["noun", "verb", "adj", "transcription"]]
#    Надо бы еще пример лямбды сюда написать, что-то не придумать как выглядит...
#    other_params : lambda p
def translate(*params):
    """
    translate - передаем слово для перевода и желаемые ключи отображения
    можно не бояться продублировать слово или запрос, перепутать их местами и тд
    если в самом ключе запроса не допущено ошибки он будет интерпретирован правильно

    умолчание:
    выведет все
    Если ни один параметр не смогли распознать - выдаем warning, печатаем умолчание
    """

    def parse_other_params(other_key):
        if other_key in requests and other_key not in request_list:
            request_list.append(other_key)

    def print_request(words, request_list):
        req_strings = []
        for request in request_list:
            req_strings.append(request)
        print "Запрос: " + ", ".join(req_strings)
        #    if is_first(request, request_list): 
        #        outstr += "\t %s" % request
        #    else:
        #        outstr += ", %s" % request
        print "=" * 60
        for word in words:
            outstr = []
            for request in request_list:
                outstr.append(dict_file[word][request])
            print word, ": ", ", ".join(outstr)
        print "=" * 60

    #def pre_load(params):
    #    result = []
    #    for param in params:
    #        if param.__type__ == "list":
    #            for
    request_list = []
    words = []
    for key in params:
        try:
            dict_file[key]
            words.append(key)
        except KeyError:
            parse_other_params(key)
    warning = len(params) > 0 and len(words) == 0 and len(request_list) > 0
    if warning:
        print "Warning: some parameters were there, but none can be parsed out"
    if len(words) == 0:
        words = dict_file.keys()
    if len(request_list) == 0:
        request_list = requests
    print_request(words, request_list)

# Примеры использования
#translate("warm","adj","rest")
#print "\n", "=" * 80
#translate("transcription")
#print "\n", "=" * 80
#translate()
#print "\n", "=" * 80
#translate("like", "transcription","sun", "noun","noun", "verb")
#print "\n", "=" * 80
#translate("blabla", "blah")
#print "\n", "=" * 80

def main(options):
    translate(*options)

if __name__ == '__main__':
    main(argv[1:])
