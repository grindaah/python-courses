try:
    res = int(open('a.txt').read())
    print res
except IOError:
    print "IOError"
except KeyboardInterrupt:
    print "Keyboard Interruption"
except:
    print "I have no idea, what happened"


def my_function():
    try:
        file_name = raw_input("Enter file name: ")
        res = open(file_name).read()
        print res
    except IOError:
        print "no file"
    except KeyboardInterrupt:
        print "KB Interrupt"
    finally:
        print "Bye-Bye"

my_function()
