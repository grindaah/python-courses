#coding: utf-8
import math

circle_d1, circle_d2, circle_d3 = 7.0,49.0,112.0

R1,R2,R3 = circle_d1 / 2.0, circle_d2 / 2.0, circle_d3 / 2.0
area_3 = math.pi * R3 ** 2


class Circle:
    x,y,radius = 0.0, 0.0, 0.0
    def __init__(self):
        pass
    def get_polar():
        D = math.sqrt(self.x*self.x + self.y * self.y)
        angle = math.acos(D / self.x)

def get_polar(x,y):
    if x == y == 0:
        return [0.0,0.0]
    D = round(math.sqrt(x*x + y*y),3)
    if x != 0:
        angle = math.acos(x / D)
    else:
        angle = math.asin(x / D)
    return D, angle

def check_enough_space(list_of_circles):
    area = 0.0
    for circle in list_of_circles:
        area += math.pi * circle[2] ** 2
    if area_3 > area:
        return True
    else:
        return False

def check_no_intersect(circle, list_of_circles):
    for ccircle in list_of_circles:
        if (circle[0] - ccircle[0]) ** 2 + (circle[1] - ccircle[1]) ** 2 < (circle[2] + ccircle[2]):
            print "oops! intersection: %s %s %s" % (circle[0] - ccircle[0], circle[1] - ccircle[1], circle[2]+ ccircle[2])
            return False
    return True

def find_next_inside(list_of_circles, last_angle, current_radius):
    #cos_alpha = circle_d1 ** 2 / (2 * circle_d1 * current_radius)
    print "current_radius = %f" % current_radius
    cos_alpha = (2 * (current_radius ** 2) - (circle_d1 ** 2)) / (2 * current_radius ** 2)
    print "cos_alpha = %.5f" % cos_alpha
    alpha = last_angle + math.acos(cos_alpha)
    print "alpha = %.5f in rad, or %.5f deg"  % (alpha, math.degrees(alpha))
    next_x = current_radius * math.cos(alpha)
    next_y = current_radius * math.sin(alpha)
    crcl = [next_x, next_y, R1]
    if check_no_intersect(crcl, list_of_circles):
        list_of_circles.append(crcl)
        for crcl in list_of_circles:
            print crcl
    else:
        return False, alpha, list_of_circles
        #find_smaller_radius()
    return True, alpha, list_of_circles

def spiral_out_keep_going(circle_list, next_angle, next_radius):
    result = True
    while result:
        result, next_angle, crcl_lst = find_next_inside(circle_list, next_angle, next_radius)
    return crcl_lst

# Нормализуем угол в пределы 360 градусов
def norm_2_2pi(angle):
    while angle > 2 * math.pi:
        angle -= 2 * math.pi
    return angle

def push_out_circle(circle_list):
    polar_list = []
    gap_list = []
    for circle in circle_list:
        print circle[0], circle[1]
        polar_list.append(get_polar(circle[0], circle[1]))
    polar_list.sort()
    biggest_gap = 0.0
    for polar in polar_list:
        index = polar_list.index(polar)
        print  index, polar
        if index+1 < polar_list.__len__():
            if polar[0] == polar_list[index+1][0]:
                gap_list.append(norm_2_2pi(polar_list[index+1][1] - polar[1]))
        else:
            if polar[1] == polar_list[0][1]:
                gap_list.append(norm_2_2pi(polar_list[index+1][1] - polar[1]))
    biggest_gap = max(gap_list)
    print "biggest_gap = %s deg" % math.degrees(biggest_gap)
    return False

def find_new_circle(bisect_angle, angle, big_R):
    sin_b = big_R / (2.0 * R1) * math.sin(angle)
    gamma = 180.0 - angle - math.asin(sin_b)
    R_x = D * math.sin(gamma) / sin_b
    return bisect_angle, R_x

crcl1 = [0.0, R3 - R1, R1]
init_lst = [crcl1]

result = True
next_angle = math.pi / 2.0
next_radius = R3 - R1

SomeMorePossible = True
while SomeMorePossible:
    pushed_circles = spiral_out_keep_going(init_lst, next_angle, next_radius)
    SomeMorePossible = push_out_circle(pushed_circles)


