import os

def print_all_files():
# we can use "walk" for example
#    for d1,d2,filenames in os.walk('.'):
#        for filename in filenames:
#            print filename
    for filename in os.listdir('.'):
        print filename


def check_txt_file(filename):
    if filename[-4:] == ".txt":
        return True
    else:
        return False

def find_python(str):
    counter = 0
    i = 1
    while i != 0:
        try:
            i = str.lower().index("python")
        except ValueError: 
            break
        counter += 1
        str = str[i+6:]

    return counter

print check_txt_file('blabla.txt')
print check_txt_file('adadtxt')

print "OK, checked it's working, going on...."
print find_python("afdpythonjklasdflkaj pythonadsflkkjfljakfsljksaf python")
print find_python("python")

counter = 0
filescounter = 0
txtfilescounter = 0

print_all_files()

for filename in os.listdir('.'):
    filescounter += 1
    if check_txt_file(filename):
        txtfilescounter += 1
        try:
            f = open(filename, 'r')
            for line in f:
                counter += line.lower().count("python")
                #find_python(line)
        except IOError:
            print "error reading file %s" % filename

print "\"python\" word is totally found %i times, total files:%i, looked through %i txt files" % (counter, filescounter, txtfilescounter)
