# coding: utf-8

def sum_list(some_list):
    count = 0
    for i in some_list:
        count += i

    return count

mylist = [0,4,5,6,4,3,3,5,6,7,8,32,4,5]

print mylist
print "Сумма = %s" % sum_list(mylist)

a,b,c = 1, 2, 3
print "Можем менять местами элементы не используя временных переменных"
print a,b,c

a,b,c = c,a,b
print a,b,c
