# coding: utf-8
from math import sqrt
from math import log
#import input

print "Simple is better then complex"

s = "abcdefghijklmnopqrstuvuwxyz"

print "Срез по 1 символу с конца и начала строки:"
while s != "":
    print s
    s = s[1:-1]

print "triangle"
a = 15
b = 45

c = sqrt(a*a + b*b)
print "%.20f" % c


print "\n--- if condition ---"

myname = "Lifagin Victor Borisovich"

for i in range(1,5):
    sometext = raw_input("enter some text, please: ")
    if len(sometext) > len(myname):
        print "Grtz"
        break
    else:
        print "no gratz, sry"
    i -= 1

# На следующем занятии мы узнаем про __sizeof__ а пока что так
def sizeof(a):
    if a == 0:
        return 1
    return int(log(a,256)) + 1

print "Расчет объема памяти, который занимает список от 1 до 10000000"
count = 0
i = 10000000
for i in xrange(1, 10000000):
    count += sizeof(i)

print "1 to 10000000 will cost you %i\t bytes" % count
print "                         or %i\t KBytes" % (count / 1024)
print "                         or %i\t\t MBytes of RAM" % (count / (1024*1024))
