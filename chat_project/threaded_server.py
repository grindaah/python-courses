#coding: utf-8
from gtk.gdk import __init__

from interface import W
import socket
import threading
import SocketServer
from PyQt4.QtGui import *
from PyQt4 import uic, QtCore, QtGui
from sys import argv
from sys import exit


# widget has to be created
Application = QApplication(argv)
widget = W()

client_addresses = []

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        """
        handler going to count "python" in received data and send it back
        """
        data = self.request.recv(1024)
        client_addresses.append(self.client_address)
        print self.client_address
        widget.connectionsList.insertItem(0, str(self.client_address))
        cur_thread = threading.current_thread()
        pyth_count = data.lower().count("python")
        response = "{}: {}, {}".format(cur_thread.name, data, pyth_count)
        self.request.sendall(response)

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

def client(ip, port, message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    try:
        sock.sendall(message)
        response = sock.recv(1024)
        print "Received: {}".format(response)
    finally:
        sock.close()

if __name__ == "__main__":


    widget.resize(480,480)
    widget.setWindowTitle("omg it's window!")
    widget.show()


    # Port 0 means to select an arbitrary unused port
    HOST, PORT = "localhost", 8880

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()
    print "Server loop running in thread:", server_thread.name

    client(ip, port, "Hello World 1 python")
    client(ip, port, "Hello World 2 pYThon python")
    client(ip, port, "Hello World 3")

    server.shutdown()

    exit(Application.exec_())