#coding: utf-8

class Py:
    pass

print Py

# В Питоне есть некие "новые" классы
# они обязательно наследуются от object

class Duck:
    def to_krya(self):
        print "krya ", self
    def __str__(self):
        return "Duck"
    def __init__(self, name="Alex"):
        self.name = name
        print self.name

duck = Duck()
duck.to_krya()

duck2 = Duck("Alx")

class Guss(Duck):
    def __init__(self, name):
        self.perra = "green"
        super(Duck,self).__init__(name)

guss = Guss()
#стандартный метод isinstance(obj, class-or-type) позволяет проверять
#print isinstance(to)


#Множественное наследование
#class A(B,C):
#    pass

#Attention: лучше не пользоваться множественным наследованием в Python

#issubclass() - проверяет подкласс ли мы

# инкапсуляция: _(имя) - protected?, __(имя) - private - и то и другое (pseudo)
