#coding: utf-8

def run_func():
    counter = 1
    while True:
        tpl = (yield)
        f = tpl[0]
        if not callable(f):
            raise Exception("First should be callable")
        params = tpl[1:]
        print "В %s-й раз вызываем" % counter
        print "Запускаю %s" % f
        f(*params)
        counter += 1

def func_(*params):
    print "Hello ", (params)

rf = run_func()
rf.next()
for i in range(1, 4):
    rf.send( (func_, i, i * 2, i ** 2) )

