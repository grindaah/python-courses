#coding: utf-8

from random import random
from random import shuffle
from random import randint

from abc import ABCMeta, abstractmethod, abstractproperty

card_par = (2,3,4,5,6,7,8,9,10,"Jack", "Queen", "King", "Ace")
lears = ("Hearts", "Spades", "Clubs", "Diamonds")


# Декоратор для функции класса, с параметром!
def lear_check(cls):
    def lear_check_decorator(lear):
        def wrapper(lear):
            if lear not in lears:
                raise LearException
            func()
        return wrapper
    return None

class TableManager(object):
    __metaclass__ = ABCMeta
    @abstractmethod
    def get_next(self):
        pass
    @abstractproperty
    def get_current(self):
        pass


#!TODO Обернуть все в декораторы lear-check
class Card(object):
    # Поменять козырь в колоде можно будет вызвав Classmethod
    @classmethod
    def change_trump(cls, lear):
        if lear in lears and lear == self.lear:
            self.trump = True

    @property
    def trump(self):
        return self.__trump

    @trump.setter
    def trump(self, x):
        self.__trump = x

    @trump.deleter
    def trump(self):
        del self.__trump

    def __init__(self, par, lear):
        if (par in card_par and lear in lears):
            self.par = par
            self.lear = lear
        self.trump = False

    def __str__(self):
        return "%s of %s" % (self.par, self.lear)

    # В документации написано, что предпочтительными являются 
    def __cmp__(self, card):
        if self.trump and card.trump:
            return card_par.index(self.par) - card_par.index(card.par)
        elif self.trump:
            return 1
        elif card.trump:
            return -1
        else:
            return card_par.index(self.par) - card_par.index(card.par)

    def __lt__(self, card):
        if self.trump and card.trump:
            return card_par.index(self.par) < card_par.index(card.par)
        elif self.trump:
            return True
        elif card.trump:
            return False
        else:
            return card_par.index(self.par) < card_par.index(card.par)

    #!TODO not zero method


class Deck:
    def __init__(self):
        self.deck = [Card(par, lear) for par in card_par for lear in lears]

    def __str__(self):
        return "Deck holds: %s cards" % len(self.deck)

    def shuffle(self):
        print "SHUFFLE" * 5
        shuffle(self.deck)

    def give_one(self):
        if len(self.deck) > 0:
            return self.deck.pop()

    @lear_check
    def set_trump(self, lear):
        if lear in lears:
            self.trump = lear
            for card in self.deck:
                card.trump = (lear == card.lear)

    def print_all(self):
        for card in self.deck:
            print card

class Hand(object):
    max_hand = 6
    def __init__(self, deck, name = "poor Alex"):
        self.name = name
        self.hand = []
        self.get_full(deck)

    def __str__(self):
        str = "Hand %s:\n" % self.name
        for card in self.hand:
            str += "%s\n" % card
        return str

    def get_full(self, deck):
        while len(self.hand) < self.max_hand:
            self.hand.append(deck.give_one())

    def get_one(self, deck):
        if len(self.hand) < self.max_hand:
            self.hand.append(deck.give_one())

class IdiotHand (Hand):
    def __init__(self, deck, name = "poor Alex"):
        Hand.__init__(self,deck, name)

    def init_a_move(self):
        print self.hand
        if len(self.hand) > 0:
            #print self.hand[0]
            #c = self.hand.sort()
            #print c #self.hand.sort(reverse = True)
            #! Attention - sort doesn't return anything - it works inplace!
            self.hand.sort()
            crd = self.hand.pop(0)
            print "%s gives: % s" % (self.name, crd)
            return crd
        else:
            print "No more cards!"

    def respond(self, card):
        posbl_list = [crd for crd in self.hand if (crd.lear == card.lear and crd.par > card.par) or (crd.trump and not card.trump)]
        if len(posbl_list) > 0:
            c = self.hand.pop(self.hand.index(posbl_list[0]))
            print "%s strikes back with: % s" % (self.name, c)
            return c
        else:
            print "%s is taking : %s" % (self.name, card)
            self.hand.append(card)

    def add(self, cards_on_table):
        posbl_list = [crd for crd in self.hand if crd.lear == card.lear or crd.trump]

    def lowest_trump(self):
        trumps = [card for card in self.hand if card.trump]
        if len(trumps) > 0:
            return trumps.sort()[0]
        else:
            return False

# !TODO decorator sorting as decorator
class IdiotGame:
    deck = Deck()
    def __init__(self, count):
        self.deck.shuffle()
        if count in range(2, 7):
            self.hands = [IdiotHand(self.deck) for hand in range(count)]
        else:
            print "Players count must be between 2 and 7"
        #for hand in self.hands:
        # !TODO get one by one algo
        #    hand.get_full(self.deck)
        self.trump = self.hands[len(self.hands)-1].hand[5]
        self.current_player = self.choose_first()
        self.next_player = self.next_player()

    def choose_first(self):
        return randint(0, len(self.hands))

    def next_player(self):
        return (self.current_player + 1) if (self.current_player + 1 < len(self.hands)) else 0

    def step(self):
        self.hands[self.next_player].respond(self.hands[self.current_player].init_a_move())

deck = Deck()
deck.shuffle()
ih = IdiotHand(deck)
print ih
ih.hand.sort(reverse = True)
print ih

i_game = IdiotGame(5)

while raw_input("press 'q' to quit, or any key to move") != 'q':
    i_game.step()


print deck
